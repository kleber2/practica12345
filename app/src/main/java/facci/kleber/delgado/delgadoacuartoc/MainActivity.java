package facci.kleber.delgado.delgadoacuartoc;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Camera;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import facci.kleber.delgado.delgadoacuartoc.menus.ActividadFragmentoColores;
import facci.kleber.delgado.delgadoacuartoc.menus.ActividadLayoutFrame;
import facci.kleber.delgado.delgadoacuartoc.menus.ActividadLayoutLinear;
import facci.kleber.delgado.delgadoacuartoc.menus.ActividadLayoutTable;
import facci.kleber.delgado.delgadoacuartoc.menus.ActividadLinterna;
import facci.kleber.delgado.delgadoacuartoc.menus.ActividadSensorAcelerometro;
import facci.kleber.delgado.delgadoacuartoc.menus.ActividadSensorLuz;
import facci.kleber.delgado.delgadoacuartoc.menus.ActividadSensorProximidad;
import facci.kleber.delgado.delgadoacuartoc.menus.ActividadVibracion;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, FrgUno.OnFragmentInteractionListener,FrgDos.OnFragmentInteractionListener{
    Button buttonLogin, buttonGuardar, buttonBuscar, buttonParametro,botonFragUno,botonFragDos,botonAutenticar,botonRegistrar;
    final static int cons = 0;
    Camera camera;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        buttonGuardar = (Button) findViewById(R.id.buttonGuardar);
        buttonBuscar = (Button) findViewById(R.id.buttonBuscar);
        buttonParametro = (Button) findViewById(R.id.btnPasarParametro);
        botonFragUno = (Button) findViewById(R.id.btnFrgUno);
        botonFragDos = (Button) findViewById(R.id.btnFrgDos);
        botonFragUno.setOnClickListener(this);
        botonFragDos.setOnClickListener(this);


        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadLogin.class);
                startActivity(intent);
            }
        });
        buttonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadRegistrar.class);
                startActivity(intent);
            }
        });
        buttonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadBuscar.class);
                startActivity(intent);
            }
        });
        buttonParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadPasarParametros.class);
                startActivity(intent);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.opcionLogin:
                intent = new Intent(MainActivity.this, ActividadLogin.class);
                startActivity(intent);
                break;
            case R.id.opcionRegistrar:
                intent = new Intent(MainActivity.this, ActividadRegistrar.class);
                startActivity(intent);
                break;
            case R.id.opcionLinear:
                intent = new Intent(MainActivity.this, ActividadLayoutLinear.class);
                startActivity(intent);
                break;
            case R.id.opcionFrame:
                intent = new Intent(MainActivity.this, ActividadLayoutFrame.class);
                startActivity(intent);
                break;
            case R.id.opcionTable:
                intent = new Intent(MainActivity.this, ActividadLayoutTable.class);
                startActivity(intent);
                break;
            case R.id.opcionColores:
                intent = new Intent(MainActivity.this, ActividadFragmentoColores.class);
                startActivity(intent);
                break;
            case R.id.opcionAcelerometro:
                intent = new Intent(MainActivity.this, ActividadSensorAcelerometro.class);
                startActivity(intent);
                break;
            case R.id.opcionProximidad:
                intent = new Intent(MainActivity.this, ActividadSensorProximidad.class);
                startActivity(intent);
                break;
            case R.id.opcionLuz:
                intent = new Intent(MainActivity.this, ActividadSensorLuz.class);
                startActivity(intent);
                break;
            case R.id.opcionCamara:
                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,cons);
                break;
            case R.id.opcionLinterna:
                intent = new Intent(MainActivity.this, ActividadLinterna.class);
                startActivity(intent);
                break;
            case R.id.opcionVibracion:
                intent = new Intent(MainActivity.this, ActividadVibracion.class);
                startActivity(intent);
               break;
            case R.id.opcionLoginD:
                Dialog dialogoLogin = new Dialog(MainActivity.this);
                dialogoLogin.setContentView(R.layout.dlg_login);
                botonAutenticar=(Button)dialogoLogin.findViewById(R.id.btnAutenticar);

                final EditText cajaUsuario=(EditText)dialogoLogin.findViewById(R.id.txtUser);
                final EditText cajaClave=(EditText)dialogoLogin.findViewById(R.id.txtPassword);
                botonAutenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this,"Usuario: "+ cajaUsuario.getText().toString()+"  Clave: "+cajaClave.getText().toString(),Toast.LENGTH_LONG).show();
                    }
                });
                dialogoLogin.show();
                break;
            case R.id.opcionRegistrarD:
                Dialog dialogoRegistrar = new Dialog(MainActivity.this);
                dialogoRegistrar.setContentView(R.layout.dlg_registrar);
                botonRegistrar=(Button)dialogoRegistrar.findViewById(R.id.btnRegistar);
                final EditText cajaNombre=(EditText)dialogoRegistrar.findViewById(R.id.txtNombre);
                final EditText cajaApellido=(EditText)dialogoRegistrar.findViewById(R.id.txtApellido);
                botonRegistrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this, "Nombre: " + cajaNombre.getText().toString() + "  Apellido: " + cajaApellido.getText().toString(), Toast.LENGTH_LONG).show();
                        }
                });
                dialogoRegistrar.show();
                break;
        }
        return true;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFrgUno:
                FrgUno fragmentoUno = new FrgUno();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor,fragmentoUno);
                transactionUno.commit();
                break;
            case R.id.btnFrgDos:
                FrgDos fragmentoDos = new FrgDos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor,fragmentoDos);
                transactionDos.commit();
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}

